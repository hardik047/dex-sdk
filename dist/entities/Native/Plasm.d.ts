import { Currency } from '../Currency';
import { NativeCurrency } from '../NativeCurrency';
import { Token } from '../Token';
/**
 * Plasm is the main usage of a 'native' currency, i.e. for Plasm mainnet and all testnets
 */
export declare class Plasm extends NativeCurrency {
    protected constructor(chainId: number);
    get wrapped(): Token;
    private static _cache;
    static onChain(chainId: number): Plasm;
    equals(other: Currency): boolean;
}

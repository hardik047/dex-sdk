import { Currency } from '../Currency';
import { NativeCurrency } from '../NativeCurrency';
import { Token } from '../Token';
/**
 * Shiden is the main usage of a 'native' currency, i.e. for Shiden mainnet and all testnets
 */
export declare class Shiden extends NativeCurrency {
    protected constructor(chainId: number);
    get wrapped(): Token;
    private static _cache;
    static onChain(chainId: number): Shiden;
    equals(other: Currency): boolean;
}
